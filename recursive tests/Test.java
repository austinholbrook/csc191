import java.util.Scanner;

class Test {
	static boolean isEq(String s1, String s2) {
		if (s1.length() != s2.length()) {
			return false;
		}

		for (int i=0; i < s1.length(); i++) {
			if (s1.charAt(i) == s2.charAt(i)) {
				return false;
			}
		}
		return true;
	}

	static boolean rIsEq(String s1, String s2) {
		if (s1.length() != s2.length()) {
			return false;
		}
		if (s1.length() == 0) {
			return true;
		}
		if (s1.charAt(0) != s2.charAt(0)) {
			return false;
		}
		return rIsEq(s1.substring(1), s2.substring(1));
	}

	static void getPermutations(String s, String start) {
		if (s.length() <= 1) {
			System.out.println(start + s);
			return;
		}

		for (int i = 0; i < s.length(); i++) {
			String startt = start + s.charAt(i);
			getPermutations(s.substring(0, i) + s.substring(i + 1), startt);
		}
	}

	public static void main(String argv[]) {
		Scanner sc = new Scanner(System.in);
		String s1, s2;
		s1 = sc.nextLine();
		//s2 = sc.nextLine();

		System.out.println("-_-_-_-_-_-");
		getPermutations(s1, "");
	}
}