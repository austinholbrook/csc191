/* Project:	Lab7
 * Name:	Austin Holbrook
 * Date: 	2014-02-26
*/

import java.util.Scanner;

class MyString {
	//Prints all possible substrings of the given string s
	static void subStrings(String s) {
		if (s.length() < 1) {
			//Done recurring
			return;
		}
		for (int i = 0; i < s.length(); i++) {
			System.out.println(s.substring(0, i+1));
		}
		subStrings(s.substring(1));
		return;
	}
}

class Lab7 {
	public static void main(String[] args) {
		//Scanner for user input
		Scanner sc = new Scanner(System.in);
		while (true) {
			System.out.print("Enter a string ('q' to quit)\n> ");
			String input = sc.nextLine();

			if (input.equals("q")) {
				System.out.println("Goodbye!");
				break;
			} else {
				MyString.subStrings(input);
			}
		}
	}
}