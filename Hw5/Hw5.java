/*
 * Author:	Austin Holbrook
 * Date:	2013-02-25
 * Program:	Hw5
 *			This program gets an integer from 1-10 from user, and prints all the
 *			factorials from n to 0 using recursion
*/

import java.util.Scanner;

class RecursionMagic {
	//Wrap printNumbers so that the user doesn't have to call it with count
	public static int printNumbers(int n) {
		return printNumbers(n, 0);
	}

	//Prints all factorials from n to 0
	private static int printNumbers(int n, int count) {
		//The indent will be 2*count in spaces
		String indent = "";
		for (int i = 0; i < count; i++) {
			indent += "  ";
		}

		//If n is 0, we need to return the factorial. We also need to print n
		//and the factorial, since the rest of the method won't be reached.
		if (n == 0) {
			System.out.println(indent + n);
			System.out.println(indent + 1);
			return 1;
		}
		System.out.println(indent + n);
		//Calling the recursive method after printing n will print all n's first
		int out = printNumbers(n-1, count+1) * n;
		System.out.println(indent + out);
		return out;
	}
}

class Hw5 {
	public static void main(String[] args) {
		//Scanner for input
		Scanner sc = new Scanner(System.in);

		//While loop to repeatedly get input and validate it.
		while (true) {
			System.out.print("Enter a number (0 - 10), or negative to quit\n> ");
			int n = sc.nextInt();
			if (n >= 0 && n <= 10) {
				RecursionMagic.printNumbers(n);
			} else if (n < 0) {
				System.out.println("Goodbye!");
				break;
			} else {
				System.out.println("Must be less than 10!");
			}
		}
	}
}